
// GLOBALS
var current_version = "1.1.1";
var title_update = "Prism2Beauty | New! v" + current_version;
var message_update = "Hey there! We release an new update, and has been automatically installed!";


function checkForValidUrl(tabId, changeInfo, tab) {
	if (tab.url !== undefined && changeInfo.status === "complete" && tab.url.indexOf('https://pte.publicisgroupe.com') === 0) {
   
    chrome.pageAction.show(tabId);

    // Angular-Material
    /*
    chrome.tabs.insertCSS(null, {"file": "vendors/angular-material/angular-material.min.css"});
    chrome.tabs.executeScript(null, {"file": "vendors/angular-material/hammer.min.js"});
    chrome.tabs.executeScript(null, {"file": "vendors/angular-material/angular.min.js"});
    chrome.tabs.executeScript(null, {"file": "vendors/angular-material/angular-animate.min.js"});
    chrome.tabs.executeScript(null, {"file": "vendors/angular-material/angular-aria.min.js"});
    chrome.tabs.executeScript(null, {"file": "vendors/angular-material/angular-material.min.js"});
    */
    //debugger;
    chrome.tabs.executeScript(null, {"file": "jquery.js"});
    chrome.tabs.executeScript(null, {"file": "jquery.linq.min.js"});
    chrome.tabs.executeScript(null, {"file": "linq.min.js"});
    chrome.tabs.executeScript(null, {"file": "app.js"});
    chrome.tabs.insertCSS(null, {"file": "style.css"});
    
  }
};

// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForValidUrl);

chrome.runtime.onUpdateAvailable.addListener(function(details){
  // Inform about new update avaible
});

// https://developer.chrome.com/extensions/runtime#event-onInstalled
chrome.runtime.onInstalled.addListener(function(details){
  if(details.reason == "update") {
    // The reason that this event is being dispatched.
    // enum of "install", "update", "chrome_update", or "shared_module_update"

    //-----> Inform the user of the sad news
    
    // This line exec open popup 
    //chrome.windows.create({url: "index.html", type: "popup"});

    // This line lunches a notification
    function notifyMe() {
      if (!Notification) {
        alert('Please us a modern version of Chrome, Firefox, Opera or Firefox.');
        return;
      }

      if (Notification.permission !== "granted"){
        Notification.requestPermission();
      }

      var notification = new Notification( title_update , {
        icon: 'beauty.png',
        body: message_update,
      });

      notification.onclick = function () {
        //window.open("http://stackoverflow.com/a/13328397/1269037");
        window.open("https://pte.publicisgroupe.com/");
      }
    }
    
    var opt = {
        // https://developer.chrome.com/apps/notifications
        type: 'list',
        title: title_update,
        message: message_update,
        priority: 1,
        items: [{ title: title_update, message: message_update}],
        iconUrl:'beauty.png'
    };

    //try {
      //chrome.notifications.create('id', opt, function(id) {});
    //} catch(e) {
      notifyMe();
    //}

  }
});

/*
	
	# other examples to use in this JS

  ////////
    
    chrome.tabs.onUpdated.addListener(function(tabId,info, tab) {
        var sites =new Array('site2','site1');
        var url=tab.url;
        var siteFlag=0;
        for(var i in sites) {
           var regexp = new RegExp('.*' + sites[i] + '.*','i');
           if (regexp.test(url)) {siteFlag=1;}
        };
        if(siteFlag==1){
          chrome.tabs.executeScript(tabId, {file:"contentscript.js"});
          chrome.tabs.executeScript(tabId, {file:"jquery.js"});
          chrome.tabs.insertCSS(tabId,{file:"box.css"});
        }
     });

  ////////




function resetDefaultSuggestion() {
  chrome.omnibox.setDefaultSuggestion({
    description: 'dapi: Search the Drupal API for %s'
  });
}

resetDefaultSuggestion();

chrome.omnibox.onInputChanged.addListener(function(text, suggest) {
  // Suggestion code will end up here.
});

chrome.omnibox.onInputCancelled.addListener(function() {
  resetDefaultSuggestion();
});

function navigate(url) {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.update(tabs[0].id, {url: url});
  });
}

chrome.omnibox.onInputEntered.addListener(function(text) {
  navigate("https://api.drupal.org/api/drupal/7/search/" + text);
});
*/ 