
'use strict';

var APP = window.APP = window.APP || {};

APP.prism = (function(){

    var isOdd = function(num){return num % 2;};

    var changeBackground = function(selector, color){
        $(selector).css("background-color" , color);
    };

    var bindEventsToUI = function() {

        //debugger;
        /*
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-58449187-1', 'auto');
            ga('send', 'pageview');
        */

        // default color values
        var colorOdd = '#FFF';
        var colorEven = '#D7F2FC';
        var colorFocus = 'LightGrey';
        var colorDot = '#cf2e3d';
        var colorOld = '';

        var rowsId = '#dgTimesheet_WeekRow';
        try {
            var numberOfRows = (document.getElementById('dgTimesheet').getElementsByTagName('tbody')[0].rows.length) -1;
            var greeting = ( (numberOfRows+1) <= 10 ) ? document.getElementById('divScroll').style.height = "200" : document.getElementById('divScroll').style.height = (numberOfRows+1) * 20;
        } catch(e){ /*console.log("Error: "+e);*/ }

        // Adding statusBox
        //$("#statusBox").remove();
        //$("#MainSection > table > tbody > tr:nth-child(2) > td").append("<div id='statusBox'> <div class='circle red'>RT</div> <div class='circle red'>OT</div> <div class='circle red'>DOT</div></div>");

        // set class for OT and  DOT
        $("td:contains('OT')").css({"color":colorDot,"font-weight":"regular"} );
        $("td:contains('DOT')").css({"color":colorDot,"font-weight":"regular"} );

        $('#dgTimesheet tbody > tr > td').hover(function() {
            var t = parseInt($(this).index()) + 1;
            $('#dgTimesheet tbody > tr > td:nth-child(' + t + '):not(:last-child):not(.grid-footer):not(.grid-footer-cell):not(.gd-it-day-disabled):not(.grid-rowtotal-cell):not(.gd-it-day-disabled-cmt):not(.gd-it-day-emp-cmt)').addClass('highlighted');
        },
        function() {
            var t = parseInt($(this).index()) + 1;
            $('#dgTimesheet tbody > tr > td:nth-child(' + t + '):not(:last-child):not(.grid-footer):not(.grid-footer-cell):not(.gd-it-day-disabled):not(.grid-rowtotal-cell):not(.gd-it-day-disabled-cmt):not(.gd-it-day-emp-cmt)').removeClass('highlighted');
        });


    };

    var getUserID = function(){
        var ID = document.getElementById('ctlBanner_lblUserInfo_nr').innerHTML;
            ID = ID.split("-")[0].split(" ")[2];
        return ID;
    };

    var updateStatus = function(){
        var frank = getUserID();
        //$.getJSON('http://191.238.228.32/prism/',{
        /*$.getJSON('http://localhost/Prism2Beauty/service/',{
            hash: frank,
            status: false
        }, function(data) {

            console.log(data.data);

        });*/

    };


    /*

    //$.getJSON('http://191.238.228.32/prism/',{
    $.getJSON('./',{
        hash: getUserID(),
        status: false
    }, function(data) {

        console.log(data.data);

        //console.log(data.dataset.oferta.length);
        //return data.dataset.oferta;
        /*
        var fuente = $('#rdo-template').html();
        var plantilla = Handlebars.compile(fuente);
        var html = plantilla(data.data.oferta);
        $('#my-container').html(html);
        */
        /*
        $.each(data.dataset.oferta, function(index, element) {
            $('body').append($('<div>', {
                text: element.asignatura
            }));
        });

    });

    var storage = "http://191.238.228.32/prism/"; // dataset URL
    var getAjaxURL = function(AppUser) {
        $.ajax({
            type: "HEAD",
            async: true,
            data: data,
            url: storage,
            success: function(message,text,response){

            }
        });

        /*
        var data = JSON.parse(localStorage.getItem("todoData"));
            localStorage.setItem("todoData", JSON.stringify(data));

            se envia por PUT
            data = {
                id : [idUserGet],
                status : [true or false] //determinado hasta el presente dia, como prism completo
            }
        */
        /*
        var request = $.ajax({
            method: 'get',
            url: ajaxUrl+AppUser+'.json',
            dataType: "json"
        }).success(function(data) {
            //console.log(data);
            return data;
        });
        return request;

    };*/

    var data = new Object();
        data = {
            "RT":[[],[],[],[],[],[],[]],
            "OT":[[],[],[],[],[],[],[]],
            "DOT":[[],[],[],[],[],[],[]]
        };

    var statusBox = function(){
        calcularHoras('RT'); //Calcula el tiempo en RT
        calcularHoras('OT'); //Calcula el tiempo en OT
        calcularHoras('DOT'); //Calcula el tiempo en DOT
        var dataJSON = JSON.stringify(data);
            //console.log(dataJSON);
    };


    var calcularHoras = function(tipoTiempo){
        var horas, codigo;
        var rowsId = '#dgTimesheet_WeekRow';
        try { var numberOfRows = (document.getElementById('dgTimesheet').getElementsByTagName('tbody')[0].rows.length) -1; } catch(e){ /*console.log("Error: "+e);*/ }
        var totHoras = 0;
        for (var i = 1; i <= numberOfRows; i++) {
            var rowId = rowsId +i;
            if($(rowId + ' td:nth-child(4) span').text().trim() === tipoTiempo){
                $(rowId + ' td:nth-child(n+8):not(.grid-rowtotal-cell)').each(function(index, value){
                    var id = "#" + $(this).parent().attr('id');
                    totHoras = parseFloat($(this).children().val());
                    codigo = $(id + " td:nth-child(2)").children().text().trim();
                    data[tipoTiempo][index].push([codigo, totHoras])
                });
            }

        }
        //console.log(totHoras);
    };

    /*
       [X] 1- Calcular que las horas dirias esten ingresadas, minimo las 8 horas
    -> [ ] 2- Bloquear los codigo RT en Sabados y Domingos
       [ ] 3- Si existen entre semana, horas OT, validar que minimo esten 9hrs RT
       [?] 4- Analytics
       [?] 5-
    */

    var fixTableHeader = function() {

        try {

            $('.grid-header').first().addClass('sticky');

            var sticky = $('.sticky'),
            stickyOffsetTop = sticky.offset().top;

            $(window).scroll(function() {
              var scrollTop = $(window).scrollTop();

              if (scrollTop >= stickyOffsetTop) {
                  sticky.addClass('fixed');
              } else {
                  sticky.removeClass('fixed');
              }

            });
                
        } catch(e){ /*console.log("Error: "+e);*/ }
        
    };

    var init = function() {
        //console.log('APP.prism2beauty');
        bindEventsToUI(); // run events
        statusBox(); // show statusBox
        fixTableHeader();
        //updateStatus(); // update service information

    };

    /**
     * interfaces to public functions
     */
    return {
        init: init
    };

}());

$( document ).ready( APP.prism.init );
//APP.prism.init();
